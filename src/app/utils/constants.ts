const appConstants =  {
  API_URL: 'https://rickandmortyapi.com/api/',
  CHARACTERS: 'character?page={page}',
  MULTIPLE_EPISODES: 'episode/{characterId}'
};

export default appConstants;
