import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import AppConstants from '../../utils/constants';
import {ApiResponse} from '../../models/apiResponse';
import {Episode} from '../../models/episode';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  public getProfilesOfCharacters(page: number): Observable<ApiResponse> {
    const url = `${AppConstants.API_URL}${AppConstants.CHARACTERS}`.replace('{page}', page.toString());
    return this.http.get<ApiResponse>(url).pipe();
  }

  public getMultipleEpisodes(characterId: string): Observable<Episode[]> {
    const url = `${AppConstants.API_URL}${AppConstants.MULTIPLE_EPISODES}`.replace('{characterId}', characterId.toString());
    return this.http.get<Episode[]>(url).pipe();
  }

  public getLocation(url: string): Observable<Location> {
    return this.http.get<Location>(url).pipe();
  }
}
