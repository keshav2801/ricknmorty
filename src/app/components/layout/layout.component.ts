import { Component, OnInit } from '@angular/core';
import {Character} from '../../models/character';
import {Episode} from '../../models/episode';
import {DataService} from '../../shared/services/data.service';
import {ApiResponse} from '../../models/apiResponse';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  characters: Character[];
  episodes: Episode[];

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.getProfiles();
  }

  private getProfiles(): void {
    this.dataService.getProfilesOfCharacters(1)
      .subscribe((data: ApiResponse) => {
        this.characters = data.results;
        this.characters.forEach(character => {
          character.episodes = this.getEpisodesIds(character).toString();
        });
      });
  }

  private getEpisodesIds(character: Character): string[] {
    const episodes: string[] = [];
    character.episode.forEach(singleEpisode => {
      const episodeSplit = singleEpisode.split('/');
      const episodeId: string = episodeSplit[episodeSplit.length - 1];
      episodes.push(episodeId);
    });
    return episodes;
  }

}
