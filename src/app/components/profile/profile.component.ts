import {Component, Input} from '@angular/core';
import {Character} from '../../models/character';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EpisodesComponent} from '../episodes/episodes.component';
import {DataService} from '../../shared/services/data.service';
import {LocationComponent} from '../location/location.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  @Input() character: Character;

  constructor(private modalService: NgbModal,
              private dataService: DataService) { }

  public openEpisodesModal(character: Character): void {
    this.getAllEpisodes(character);
  }

  private getAllEpisodes(character: Character): void {
    this.dataService.getMultipleEpisodes(character.episodes)
      .subscribe(data => {
        const modalRef = this.modalService.open(EpisodesComponent);
        if (Array.isArray(data)) {
          modalRef.componentInstance.episodes = data;
        } else {
          modalRef.componentInstance.episodes = [data];
        }
        modalRef.componentInstance.currentCharacter = character;
      });
  }

  public getCharacterLocationInfo(character: Character): void {
    this.dataService.getLocation(character.location.url)
      .subscribe(data => {
        const modalRef = this.modalService.open(LocationComponent);
        modalRef.componentInstance.location = data;
      });
  }

}
