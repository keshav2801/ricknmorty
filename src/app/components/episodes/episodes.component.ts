import { Component } from '@angular/core';
import {Episode} from '../../models/episode';
import {Character} from '../../models/character';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.css']
})
export class EpisodesComponent {
  episodes: Episode[];
  currentCharacter: Character;

  constructor(private modalService: NgbModal) { }

  closePopup(): void {
    this.modalService.dismissAll();
  }

}
