import { Component } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CharacterLocation} from '../../models/characterLocation';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent {
  public location: CharacterLocation;

  constructor(private modalService: NgbModal) { }

  public closePopup(): void {
    this.modalService.dismissAll();
  }

}
