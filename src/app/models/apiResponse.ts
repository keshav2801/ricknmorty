import {Character} from './character';

export class ApiResponse {
  info: any;
  results: Character[];
}
